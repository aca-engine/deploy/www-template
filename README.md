# https://bitbucket.org/aca/www-template
Is a base www directory which can be forked for each new deployment/client. Generally placed in /home/aca-apps/www on prod and uat web servers and contains minified final production builds of:
- backoffice/
- meeting/
- bookings/
- staff/
- catering/
and any other client-branded/custom frontends for this particular client

When pushing, please ensure that any environment related settings (e.g. staff/assets/settings.json) match the actual production setting that is required. UAT settings can go into a `uat` branch

# Usage
* Treat `master` branch as the one to deploy to the UAT (or POC) server(s), including having any settings.json values up match the UAT environment requirements.
* Treat `prod` branch as the one to deploy to the PROD server(s), including having any settings.json values up match the PROD environment requirements.


### Updating forks from this repo ###
NOTE: that this will attempt to overwrite any of your repo's changes (e.g. to /staff/), but will result in a merge conflict. To merge in specific commits (cherry-pick) see below.
In your fork:
```
git remote add www-template https://bitbucket.org/aca/www-template
git remote set-url --push www-template no-pushing-allowed  #Just to ensure you don't push your private fork to www-template
git pull -X theirs www-template master
git push origin master
```

### Cherry picking specific commits from this repo ###
In your fork:
```
git remote add www-template https://gitlab.com/aca-engine/deploy/www-template.git
git remote set-url --push www-template no-pushing-allowed  #Just to ensure you don't push your private fork to www-template
git fetch www-template
git cherry-pick <COMMIT-HASH OR TAG>
git push origin master
```

#### e.g. To apply the fix from March 2019 that fixes Backoffice clicking checkbox/link issues in Chrome
```
git clone <www-client-repo>
cd <www-client-repo>
git remote add www-template https://gitlab.com/aca-engine/deploy/www-template.git
git remote set-url --push www-template no-pushing-allowed
git fetch www-template
git cherry-pick fix/backoffice/click/chrome73
git push origin master
```



# Auto login for fixed panels (e.g. booking panels, kiosks, signage)

## Options for enabling automatic login

All ACAEngine web apps requires authentication, this is usually done by the user signing in on their laptop/phone using their own personal account.
However, some endpoints are managed and public and need to AUTOMATICALLY sign in with a role based account. Examples are room booking panels, digital signage and kiosks.

The best method to allow managed endpoints to auto login is to use SSO via a certificate that's installed to each endpoint. With this method, no special configuration is required and the endpoint should automatically be redirected to login when visiting the normal web app url. 

However, for endpoints without certificate based SSO configurd, a "panel login" url can be used to automate the login process.

## panel_login.html url
The panel login page ensures that credentials are not stored in any application resources (html/js) by setting the username and password be set as a base64 encoded query parameters in the url.
The resulting panel login url should be considered *SECRET*.

### a) Creating the panel_login url

A final panel login url (that can be set on endpoint devices such as ipads and android tablets) will usually be in the below format:

`https://ENGINE_URL/panel_login.html?u=<ENCODED_CREDENTIALS>&continue=/<APPLICATION>/`

where `<APPLICATION>` is the target frontend app (e.g. `bookings`)
and `ENCODED_CREDENTIALS` is the below string but [BASE 64 ENCODED](https://www.base64encode.net/):
`u=<ACCOUNT_EMAIL>&p=<PASSWORD>`

1. Ensure that a role based account with a matching email and UNIQUE password has been created on the same domain via BACKOFFICE.
1. If the role based account email is kiosk@aca.im and password is 14e43wrt123e3, then the string we want to [encode to BASE64](https://www.base64encode.net/) is: "u=kiosk@aca.im&p=14e43wrt123e3"
1. The resulting BASE64 encoded string is dT1raW9za0BhY2EuaW0mcD0xNGU0M3dydDEyM2Uz and so finally, the resulting panel login url is:
    `https://ENGINE_URL/panel_login.html?u=dT1raW9za0BhY2EuaW0mcD0xNGU0M3dydDEyM2Uz&continue=/<APPLICATION>/`

### b) Deploying the panel login url to endpoints

When a browser loads a panel login url, it will:
1. First, attempt to login using the encoded credentials in the URL and if successful will wait 10seconds (to allow a user to bookmark the url or add to homescreen) and THEN
2. Full page redirect to the browser the APPLICATION url.

So when setting the application URL or pinning the app to the homescreen, it is very important to set or pin the url in *step 1* NOT step 2. If step 2 is pinned, the endpoint may end up logged out in the future. 

#### iPads
So, for manually managed iPads with Safari, ensure the "Add to Homescreen" option is selcted while the page is white and still showing "*Authenticating...*"
If the screen is already showing the APP (e.g. booking panel) instead of the white panel login screen, then it is too late to pin the app.

While panel login is authenticating...

![panel_login.html](.assets/panel_login_authenticating.jpg "While panel login is authenticating...")

Add the url to the homescreen:

![iOS: Add to Homescreen](.assets/ios_add_to_homescreen.png "Add the url to the homescreen")