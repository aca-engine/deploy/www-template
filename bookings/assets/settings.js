var SETTINGS = {
    env: 'prod',
    debug: false,
    mock: false,
    composer: {},
    app: {},
};

/*=======================*\
||   COMPOSER SETTINGS   ||
\*=======================*/

SETTINGS.composer = {
    domain: '',
    route: '/bookings',
    protocol: '',
    use_domain: false,
    local_login: false,
};

/*==========================*\
||   APPLICATION SETTINGS   ||
\*==========================*/

SETTINGS.app = {
    title: 'PlaceOS',
    name: 'PlaceOS',
    description: 'PlaceOS Frontend made in Angular 9.1+',
    short_name: 'Bookings',
    code: 'BOOK',
    copyright: 'Copyright 2018 Place Technology',
    login: {
        forgot: false,
    },
    analytics: {
        enabled: false,
        tracking_id: '',
    },
    logo_light: {
        type: 'img',
        src: 'assets/img/logo.svg',
        background: '',
    },
    topbar: false,
    show_status_when_disconnected: false,
};

/*==========================*\
||   GENERAL SETTINGS   ||
\*==========================*/

SETTINGS.app.bookings = {
    title_prefix: '',
    fields: [
        {
            key: 'space',
            type: 'input',
            label: 'Space',
            hide: true,
        },
        {
            key: 'host',
            type: 'custom',
            label: 'Host',
        },
        {
            key: 'date',
            type: 'custom',
            label: 'Date',
            hide: true,
        },
        {
            key: 'period',
            type: 'group',
            children: [
                {
                    key: 'time',
                    type: 'custom',
                    label: 'Start time',
                },
                {
                    key: 'duration',
                    type: 'custom',
                    label: 'Duration',
                },
            ],
        },
        {
            key: 'title',
            type: 'input',
            label: 'Title',
        },
    ],
};

/*==========================*\
||      USER SETTINGS     ||
\*==========================*/

SETTINGS.app.users = {
    enabled: false,
    search: false,
};

/*==========================*\
||      ROOM SETTINGS     ||
\*==========================*/

SETTINGS.app.rooms = {
    enabled: false,
};

/*==========================*\
||       ZONE SETTINGS      ||
\*==========================*/

SETTINGS.app.zones = {};

/*==========================*\
||      DRIVER SETTINGS     ||
\*==========================*/

SETTINGS.app.drivers = {};

/** Add settings to the global space */
window['settings.json'] = SETTINGS;

console.log('Embbeded settings.json');
